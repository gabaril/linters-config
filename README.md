# Linters Config

## TSLINT

1. Install TSLINT in your project
   `npm install tslint typescript --save-dev`
2. Generate basic configuration file
   `tslint --init`
3. Replace the new tslint.json content with the one from the repo

## PRETTIER

1. Install prettier in your project
   `npm install --save-dev --save-exact prettier`
2. Create `.prettierrc` and `.prettierignore` files
3. Replace those files content with the ones from the repo
4. Install tslint-config-prettier
   `npm install --save-dev tslint-config-prettier`
5. Add `"tslint-check": "tslint-config-prettier-check ./tslint.json"` to the scripts section of your package.json
6. Run the above script to make sure no conflicts are present in your configuration

## IDE

### VSCODE

1. Activate tslint
[TSLint - Visual Studio Marketplace](https://marketplace.visualstudio.com/items?itemName=eg2.tslint)
2. Activate Prettier
[Prettier - Code formatter - Visual Studio Marketplace](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode) 
3. Configure Prettier for autoformat on save
[GitHub - prettier/prettier-vscode: Visual Studio Code extension for Prettier](https://github.com/prettier/prettier-vscode)

### PHPSTORM

1. Activate tslint
[TSLint - Help | PhpStorm](https://www.jetbrains.com/help/phpstorm/tslint.html)
2. Activate prettier
[Prettier - Plugins | JetBrains](https://plugins.jetbrains.com/plugin/10456-prettier)
